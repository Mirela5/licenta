import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  show = false;
  constructor() { }

  ngOnInit(): void {
  }

  public data: Object[] = [
    { x: 1, y: 45, text: 'USA', fill: '#00226C', size: 1.347 },
    { x: 2, y: 20, text: 'AUS', fill: '#0450C2', size: 1 },
    { x: 3, y: 56, text: 'CHN', fill: '#0073DC', size: 0.01 },
    { x: 4, y: 30, text: 'IND', fill: '#0D98FF', size: 0.7 },
    { x: 5, y: 13, text: 'JPN', fill: '#9CD9FF', size: 0.333 },
    { x: 6, y: 45, text: 'UK', fill: '#0450C2', size: 1 }
];

}
