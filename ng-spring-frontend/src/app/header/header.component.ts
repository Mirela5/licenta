import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService} from '../menu.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  _opened: boolean = false;
  
  constructor(private router: Router, public menuService: MenuService) { }

  ngOnInit(): void {
  }

  onClickHome(){
    console.log("home");
    this.router.navigate(['/home'])
  }

  _toggleSidebar() {
    this._opened = !this._opened;
  }  
}
