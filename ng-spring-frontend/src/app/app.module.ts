import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { InputComponent } from './input/input.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PasswordComponent } from './password/password.component';
import { ChartsModule } from 'ng2-charts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import * as $ from 'jquery';
// for HttpClient import:
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
// for Router import:
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
// for Core import:
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { StatisticsComponent } from './statistics/statistics.component';
import { HomeComponent } from './home/home.component';
import { SidebarModule } from 'ng-sidebar';
import { ChartModule } from '@syncfusion/ej2-angular-charts';
import { BubbleSeriesService, CategoryService} from '@syncfusion/ej2-angular-charts';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InputComponent,
    PasswordComponent,
    StatisticsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    AngularMultiSelectModule,
    RouterModule.forRoot(
      [ {path: 'input',component: InputComponent},
        {path: 'password', component: PasswordComponent},
        {path: 'statistics', component: StatisticsComponent},
        {path: 'home', component: HomeComponent}
      ]
    ),
    HttpClientModule,
    ChartsModule,
    LoadingBarHttpClientModule,

    // for Router use:
    LoadingBarRouterModule,

    // for Core use:
    LoadingBarModule,
    SidebarModule.forRoot(),
    BrowserModule, ChartModule
 
  ],

  providers: [BubbleSeriesService, CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
