import { Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PasswordPayload} from './password-payload';
import {InputServiceService} from './input-service.service';
import { bruteForcePayload } from './bruteForcePayload';
import { Observable } from 'rxjs';
import {Chart} from 'node_modules/chart.js' 
import { NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {analyzeAndValidateNgModules} from '@angular/compiler'
import { Label, SingleDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';
import jsPDF from 'jspdf';
import * as $ from 'jquery';
import html2canvas from 'html2canvas';
import html2pdf from 'html2pdf.js';
import { Router } from "@angular/router";

@Component({
  selector: 'app-input', 
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})

export class InputComponent implements OnInit {

  registerForm: FormGroup;
  passwordPayload: PasswordPayload;
  showMe: boolean = false;
  count: any = 0;
  itemList = [];
  selectedItems = [];
  settings = {};
  
  public radarChartLabels = ['Q1', 'Q2', 'Q3', 'Q4'];
  public radarChartData = [
    {data: [120, 130, 180, 70], label: 'Attack Module'},
    {data: [90, 150, 200, 45], label: 'Heuristics Module'}
  ];
  public radarChartType = 'radar';
  reportContent: any;


  constructor(private formBuilder: FormBuilder, private inputService: InputServiceService, private router: Router) {
    this.registerForm=this.formBuilder.group( {username : '' });
    this.passwordPayload={password: ''};
  }

  ngOnInit() {
    this.itemList = [
      { "id": 1, "itemName": "Attack Module" },
      { "id": 2, "itemName": "Heuristics Module" },
      { "id": 3, "itemName": "Adaptative Memory" },
      { "id": 4, "itemName": "HierarchicalMC" }
  ];

  this.selectedItems =[
    { "id": 1, "itemName": "Attack Module" },
    { "id": 2, "itemName": "Heuristics Module" },
    { "id": 3, "itemName": "Adaptative Memory" },
    { "id": 4, "itemName": "HierarchicalMC" }
];
  this.settings = {
      text: "Select Modules",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "myclass custom-class"
  };
}



onItemSelect(item: any) {
  console.log(item);
  console.log(this.selectedItems);
}
OnItemDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedItems);
}
onSelectAll(items: any) {
  console.log(items);
}
onDeSelectAll(items: any) {
  console.log(items);
}


  onSubmit() {
    this.count += 1 ;
    this.passwordPayload.password = this.registerForm.get('username').value;
    console.log(this.passwordPayload.password);
    this.inputService.addPassword(this.passwordPayload).subscribe(data => { console.log('register success');}, 
      error => { console.log("register failed"); });
      console.log(this.inputService.getConfig());
    
      if( this.count % 2 == 1 )
          this.showMe = true;
      else
          this.showMe = false;

          this.router.navigate(['/password'])
  }

  myFunction(id:String){
    console.log("AICI");
    console.log(id);
  }
  
  onDownload(){
    /*html2canvas(document.body).then(canvas => {
      var pdf = new jsPDF('l', 'mm', 'a4');
      
      pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 300,150);
      pdf.save();
  });*/
  const data = document.getElementById('chart');
                 html2canvas(document.body).then(canvas => {
                   const imgWidth = 300;
                   const pageHeight = 295;
                   const imgHeight = (canvas.height * imgWidth) / canvas.width;
                   const heightLeft = imgHeight;
                   const contentDataURL = canvas.toDataURL('image/png');
                   const pdf = new jsPDF('p', 'mm', 'a4');
                   const position = 0;
                   pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, position, imgWidth,imgHeight);
                   pdf.save(name);
                   })
                  
}
}
