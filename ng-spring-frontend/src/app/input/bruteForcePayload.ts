export class bruteForcePayload{
    id: String;
    dictionaryScore: String;
    hit: String;
    levensteinDistance: String;
}