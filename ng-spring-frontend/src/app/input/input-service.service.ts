import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { PasswordPayload } from './password-payload';
import { bruteForcePayload } from './bruteForcePayload';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class InputServiceService {

  private url = 'http://localhost:8080/input/';
  
  constructor(private httpClient: HttpClient) { }

  addPassword(passwordPayload: PasswordPayload): Observable<any>{
    console.log(passwordPayload);
    return this.httpClient.post(this.url + "password", passwordPayload);
  }

  getConfig() {
    console.log("aici");
    return this.httpClient.get<Array<bruteForcePayload>>(this.url);
  }
}
