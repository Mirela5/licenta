import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  employeeId : String

  constructor(private route : ActivatedRoute) { 
    
  }

  ngOnInit(){
   
   }

   onDownload(){
    /*html2canvas(document.body).then(canvas => {
      var pdf = new jsPDF('l', 'mm', 'a4');
      
      pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 300,150);
      pdf.save();
  });*/
  const data = document.getElementById('chart');
                 html2canvas(document.body).then(canvas => {
                   const imgWidth = 300;
                   const pageHeight = 295;
                   const imgHeight = (canvas.height * imgWidth) / canvas.width;
                   const heightLeft = imgHeight;
                   const contentDataURL = canvas.toDataURL('image/png');
                   const pdf = new jsPDF('p', 'mm', 'a4');
                   const position = 0;
                   pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, position, imgWidth,imgHeight);
                   pdf.save(name);
                   })
                  
}
}
