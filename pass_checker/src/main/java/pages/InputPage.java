package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InputPage {

    private WebDriver driver;
    private By testPassword = By.xpath("/html/body/app-root/app-input/div/div/div/form/button");

    public InputPage(WebDriver driver) {
        this.driver = driver;
    }

    public PasswordPage clickTestPassword (){
        driver.findElement(testPassword).click();
        return new PasswordPage(driver);
    }

}
