package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;
    private By testPasswordButton = By.xpath("/html/body/app-root/app-home/div/div[2]/button");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public InputPage clickTestPasswordButton (){
        driver.findElement(testPasswordButton).click();
        return new InputPage(driver);
    }
}
