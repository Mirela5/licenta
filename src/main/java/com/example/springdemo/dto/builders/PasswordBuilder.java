package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PasswordDTO;
import com.example.springdemo.entities.Password;

public class PasswordBuilder {
    private PasswordBuilder(){}
    public static PasswordDTO generateDTOFromEntity(Password item) {
        return new PasswordDTO(
                item.getId(),
                item.getPassword(),
                item.getUtilDate());
    }

    public static Password generateEntityFromDTO(PasswordDTO itemDTO) {
        return new Password(
                itemDTO.getId(),
                itemDTO.getPassword(),
                itemDTO.getUtilDate());
    }
}
