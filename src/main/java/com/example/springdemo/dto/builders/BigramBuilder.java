package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.BigramDTO;
import com.example.springdemo.entities.Bigram;

public class BigramBuilder {

    private BigramBuilder(){}
    public static BigramDTO generateDTOFromEntity(Bigram item) {
        return new BigramDTO(
                item.getSequence(),
                item.getBigramFrequency());
    }

    public static Bigram generateEntityFromDTO(BigramDTO itemDTO) {
        return new Bigram(
                itemDTO.getSequence(),
                itemDTO.getFrequency());
    }
}
