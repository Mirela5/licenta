package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.HeuristicsDTO;
import com.example.springdemo.entities.Heuristics;

public class HeuristicsBuilder {

    private HeuristicsBuilder() {
    }

    public static HeuristicsDTO generateDTOFromEntity(Heuristics item) {
        return new HeuristicsDTO(
                item.getId(),
                item.getN(),
                item.getNoGmax(),
                item.getTmax(),
                item.getHeuristicsScore());
    }

    public static Heuristics generateEntityFromDTO(HeuristicsDTO itemDTO) {
        return new Heuristics(
                itemDTO.getId(),
                itemDTO.getN(),
                itemDTO.getNoGmax(),
                itemDTO.getTmax(),
                itemDTO.getHeuristicsScore());
    }
}
