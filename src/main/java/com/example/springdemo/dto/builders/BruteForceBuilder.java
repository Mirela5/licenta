package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.BruteForceDTO;
import com.example.springdemo.entities.BruteForce;

public class BruteForceBuilder{

    private BruteForceBuilder() {
    }

    public static BruteForceDTO generateDTOFromEntity(BruteForce item) {
        return new BruteForceDTO(
                item.getId(),
                item.getDictionaryScore(),
                item.getHit(),
                item.getLevensteinDistance());
    }

    public static BruteForce generateEntityFromDTO(BruteForceDTO itemDTO) {
        return new BruteForce(
                itemDTO.getId(),
                itemDTO.getDictionaryScore(),
                itemDTO.getHit(),
                itemDTO.getLevensteinDistance());
    }
}
