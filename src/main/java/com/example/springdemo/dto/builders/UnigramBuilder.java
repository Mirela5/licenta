package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.UnigramDTO;
import com.example.springdemo.entities.Unigram;

public class UnigramBuilder {

    private UnigramBuilder() {
    }

    public static UnigramDTO generateDTOFromEntity(Unigram item) {
        return new UnigramDTO(
                item.getSequence(),
                item.getFrequency());
    }

    public static Unigram generateEntityFromDTO(UnigramDTO itemDTO) {
        return new Unigram(
                itemDTO.getSequence(),
                itemDTO.getFrequency());
    }
}
