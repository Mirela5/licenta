package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.TrigramDTO;
import com.example.springdemo.entities.Trigram;

public class TrigramBuilder {

    private TrigramBuilder(){}
    public static TrigramDTO generateDTOFromEntity(Trigram item) {
        return new TrigramDTO(
                item.getSequence(),
                item.getFrequency());
    }

    public static Trigram generateEntityFromDTO(TrigramDTO itemDTO) {
        return new Trigram(
                itemDTO.getSequence(),
                itemDTO.getFrequency());
    }
}
