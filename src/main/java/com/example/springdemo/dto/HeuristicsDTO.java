package com.example.springdemo.dto;

public class HeuristicsDTO {

    private Integer id;
    private Integer Tmax;
    private Integer N;
    private long NoGmax;
    private Integer heuristicsScore;

    public HeuristicsDTO(Integer id, Integer n, long noGmax, Integer tmax, Integer heuristicsScore){
        this.id = id;
        this.Tmax = tmax;
        this.N = n;
        this.NoGmax = noGmax;
        this.heuristicsScore = heuristicsScore;
    }
    public HeuristicsDTO(){}


    public HeuristicsDTO(Integer tmax, Integer n, long noGmax, Integer heuristicsScore) {
        this.Tmax = tmax;
        this.N = n;
        this.NoGmax = noGmax;
        this.heuristicsScore = heuristicsScore;
    }


    public Integer getHeuristicsScore() {
        return heuristicsScore;
    }

    public void setHeuristicsScore(Integer heuristicsScore) {
        this.heuristicsScore = heuristicsScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTmax() {
        return Tmax;
    }

    public void setTmax(Integer tmax) {
        Tmax = tmax;
    }

    public Integer getN() {
        return N;
    }

    public void setN(Integer n) {
        N = n;
    }

    public long getNoGmax() {
        return NoGmax;
    }

    public void setNoGmax(long noGmax) {
        NoGmax = noGmax;
    }

}
