package com.example.springdemo.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PasswordDTO {

    private Integer id;
    private String password;
    private Date utilDate;

    public PasswordDTO(){}

    public PasswordDTO(Integer id, String password, Date utilDate) {
        this.id = id;
        this.password = password;
        this.utilDate = utilDate;
    }

    public Date getUtilDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return date;
    }

    public void setUtilDate(Date utilDate) {
        this.utilDate = utilDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
