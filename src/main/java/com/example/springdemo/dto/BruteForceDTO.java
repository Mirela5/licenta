package com.example.springdemo.dto;
import java.util.ArrayList;

public class BruteForceDTO {

    private Integer id;
    private  double dictionaryScore;
    private Integer hit;
    private Integer levensteinDistance;


    public BruteForceDTO(){}

    public BruteForceDTO(Integer id,  double dictionaryScore, Integer hit,Integer levensteinDistance) {
        this.id = id;
        this.dictionaryScore = dictionaryScore;
        this.hit = hit;
        this.levensteinDistance = levensteinDistance;
    }

    public BruteForceDTO( double dictionaryScore, Integer hit, Integer levensteinDistance) {
        this.dictionaryScore = dictionaryScore;
        this.hit = hit;
        this.levensteinDistance = levensteinDistance;
    }

    public Integer getId() {
        return id;
    }

    public Integer getLevensteinDistance() {
        return levensteinDistance;
    }

    public void setLevensteinDistance(Integer levensteinDistance) {
        this.levensteinDistance = levensteinDistance;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public  double getDictionaryScore() {
        return dictionaryScore;
    }

    public void setDictionaryScore(double dictionaryScore) {
        this.dictionaryScore = dictionaryScore;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }
}
