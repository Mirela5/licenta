package com.example.springdemo.dto;

public class UnigramDTO {

    private String sequence;
    private Integer frequency;

    public UnigramDTO(){}

    public UnigramDTO(String sequence, Integer frequency) {
        this.sequence = sequence;
        this.frequency = frequency;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

}
