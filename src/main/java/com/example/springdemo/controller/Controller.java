package com.example.springdemo.controller;

import com.example.springdemo.dto.BruteForceDTO;
import com.example.springdemo.dto.PasswordDTO;
import com.example.springdemo.entities.*;
import com.example.springdemo.services.*;
import com.example.springdemo.services.AttackModule.DictionaryAttack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/input")
public class BruteForceController {
    @Autowired
    private final BruteForceService bruteForceService;
    @Autowired
    private final PasswordService passwordService;
    @Autowired
    private final HeuristicsService heuristicsService;
    @Autowired
    private final MarkovChainService markovChainService;

    @Autowired
    public BruteForceController(BruteForceService bruteForceService, PasswordService passwordService, HeuristicsService heuristicsService, MarkovChainService markovChainService)
    {
        this.bruteForceService = bruteForceService;
        this.passwordService = passwordService;
        this.heuristicsService = heuristicsService;
        this.markovChainService = markovChainService;
    }

    @GetMapping(value = "/{id}")
    public BruteForceDTO findById(@PathVariable("id") Integer id){
        return bruteForceService.findPersonById(id);
    }

    @GetMapping("/grafic1")
    public List<BruteForceDTO> findAll(){
        return bruteForceService.findAll();
    }

    @PostMapping("/email")
    public ResponseEntity sendEmail(@RequestBody String email){
        passwordService.sendEmail(email);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/heuristicsbar")
    public List<Integer> sendBarHeuristics(){
        System.out.println("Controller");
        return heuristicsService.findHeuristicsScore();
    }

    @GetMapping("/fusion_score")
    public List<Integer> sendScore(){
        return passwordService.sendFusionScore(); }

    @GetMapping("/scores_metrics")
    public List<Integer> sendScoreMetrics(){
        return passwordService.sendScoresMetrics(); }

    @GetMapping("/scores_attack")
    public List<Integer> sendScoreAttack(){
        return passwordService.sendAttackScore(); }

    @GetMapping("/scores_heuristics")
    public List<Integer> sendScoreHeuristics(){
        return passwordService.sendHeuristics(); }

    @GetMapping("/scores_adaptive")
    public List<Integer> sendScoreAdaptive(){
        return passwordService.sendAdaptive(); }

    @GetMapping("/heuristicspie")
    public List<Integer> sendPieHeuristics(){
        return heuristicsService.findCharacters();
    }

    @GetMapping("/length")
    public List<Length> sendLenght(){
        return heuristicsService.findLength();
    }

    @GetMapping("/unigram")
    public List<Unigram> sendUnigram(){ return markovChainService.findUnigram(); }

    @GetMapping("/pie")
    public List<Integer> sendPie(){
        return bruteForceService.findSimilarity();
    }

    @GetMapping("/unigram2")
    public List<Unigram> sendBarAdaptive(){
        return markovChainService.findTop();
    }

    @GetMapping("/adaptivepie")
    public List<Integer> sendPieAdaptive(){
        return markovChainService.findCategories();
    }

    @GetMapping("/bar")
    public List<Integer> sendBar(){
        return bruteForceService.findHitMiss();
    }

    @GetMapping("/testFinall")
    public Integer sendTest(){
        return markovChainService.testFinal();
    }

    @PostMapping("/search")
    public ResponseEntity sendPasswordToSearch(@RequestBody PasswordSearch pass){
        passwordService.sendPasswordToSearch(pass);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/find")
    public List<Double> findPassword(){
        return passwordService.findPasswordBranch();
    }

    @PostMapping("/password")
    public Integer insertPasswordDTO(@RequestBody PasswordDTO bruteForceDTO){

        return passwordService.insert(bruteForceDTO);
    }

    /*@PostMapping()
    public Integer insertFromDictionary(@RequestBody DictionaryAttack dictionaryAttack){

        dictionaryAttack.readFileInList();
        ArrayList<String> dic = dictionaryAttack.getDictionary();
        BruteForceDTO brute = dictionaryAttack.getStrenght("password", dic);
        return bruteForceService.insert(brute);
    }*/

   /* @PutMapping("/delete")
    public Integer updatePerson(@RequestBody BruteForceDTO bruteForceDTO) {
        return bruteForceService.update(bruteForceDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PersonViewDTO personViewDTO){
        personService.delete(personViewDTO);
    }*/
    /*@PostMapping("/add")
    @ResponseStatus(HttpStatus.OK)
    public String addUser(@Valid @RequestBody  BruteForce bruteForce) {
        bruteForceRepository.save(bruteForce);
        return"Succes";
    }*/
}


/*
* private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = "/{id}")
    public PersonViewDTO findById(@PathVariable("id") Integer id){
        return personService.findPersonById(id);
    }

    @GetMapping()
    public List<PersonViewDTO> findAll(){
        return personService.findAll();
    }

    @GetMapping(value = "/items")
    public List<PersonWithItemsDTO> findAllWithItems(){
        return personService.findAllFetch();
    }

    @GetMapping(value = "/items/wrong")
    public List<PersonWithItemsDTO> findAllWithItemsWrong(){
        return personService.findAllFetchWrong();
    }

    @PostMapping()
    public Integer insertPersonDTO(@RequestBody PersonDTO personDTO){
        return personService.insert(personDTO);
    }

    @PutMapping()
    public Integer updatePerson(@RequestBody PersonDTO personDTO) {
        return personService.update(personDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PersonViewDTO personViewDTO){
        personService.delete(personViewDTO);
    }
}
*
* */