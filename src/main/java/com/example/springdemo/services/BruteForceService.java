package com.example.springdemo.services;
import com.example.springdemo.dto.BruteForceDTO;
import com.example.springdemo.dto.builders.BruteForceBuilder;
import com.example.springdemo.entities.BruteForce;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.BruteForceRepository;
import com.example.springdemo.services.AttackModule.DictionaryAttack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
public class BruteForceService {

    @Autowired
    private BruteForceRepository bruteForceRepository;
    //private DictionaryAttack dictionary1= new DictionaryAttack(this);


    public BruteForceDTO findPersonById(Integer id){
        Optional<BruteForce> bruteForce  = bruteForceRepository.findById(id);

        if (!bruteForce.isPresent()) {
            throw new ResourceNotFoundException("BruteForce", "id", id);
        }
        return BruteForceBuilder.generateDTOFromEntity(bruteForce.get());
    }

    public List<BruteForceDTO> findAll(){
        List<BruteForce> bruteForce = bruteForceRepository.getAllOrdered();

        return bruteForce.stream()
                .map(BruteForceBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }



    public Integer insert(BruteForceDTO bruteForceDTO) {

        BruteForce brute = new BruteForce();
        brute.setId(bruteForceDTO.getId());
        brute.setDictionaryScore(bruteForceDTO.getDictionaryScore());
        brute.setHit(bruteForceDTO.getHit());
        return bruteForceRepository
                .save(BruteForceBuilder.generateEntityFromDTO(bruteForceDTO))
                .getId();
    }

    /*public Integer update(PersonDTO personDTO) {

        Optional<Person> person = personRepository.findById(personDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Person", "user id", personDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return personRepository.save(PersonBuilder.generateEntityFromDTO(personDTO)).getId();
    }

    public void delete(PersonViewDTO personViewDTO){
        this.personRepository.deleteById(personViewDTO.getId());
    }
*/



}

/*
* private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public PersonViewDTO findPersonById(Integer id){
        Optional<Person> person  = personRepository.findById(id);

        if (!person.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", id);
        }
        return PersonViewBuilder.generateDTOFromEntity(person.get());
    }

    public List<PersonViewDTO> findAll(){
        List<Person> persons = personRepository.getAllOrdered();

        return persons.stream()
                .map(PersonViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PersonWithItemsDTO> findAllFetch(){
        List<Person> personList = personRepository.getAllFetch();

        return personList.stream()
                .map(x-> PersonWithItemsBuilder.generateDTOFromEntity(x, x.getItems()))
                .collect(Collectors.toList());
    }


    //WRONG - without fetch an additional query is executed for each FK
    public List<PersonWithItemsDTO> findAllFetchWrong(){
        List<Person> personList = personRepository.findAll();

        return personList.stream()
                .map(x-> PersonWithItemsBuilder.generateDTOFromEntity(x, x.getItems()))
                .collect(Collectors.toList());
    }

    public Integer insert(PersonDTO personDTO) {
        PersonFieldValidator.validateInsertOrUpdate(personDTO);

        Person person = personRepository.findByEmail(personDTO.getEmail());
        if(person != null){
            throw  new DuplicateEntryException("Person", "email", personDTO.getEmail());
        }

        return personRepository
                .save(PersonBuilder.generateEntityFromDTO(personDTO))
                .getId();
    }

    public Integer update(PersonDTO personDTO) {

        Optional<Person> person = personRepository.findById(personDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Person", "user id", personDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return personRepository.save(PersonBuilder.generateEntityFromDTO(personDTO)).getId();
    }

    public void delete(PersonViewDTO personViewDTO){
        this.personRepository.deleteById(personViewDTO.getId());
    }

* */