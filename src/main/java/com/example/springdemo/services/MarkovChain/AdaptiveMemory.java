package com.example.springdemo.services.MarkovChain;

import com.example.springdemo.dto.BigramDTO;
import com.example.springdemo.dto.TrigramDTO;
import com.example.springdemo.dto.UnigramDTO;
import com.example.springdemo.dto.builders.BigramBuilder;
import com.example.springdemo.entities.Bigram;
import com.example.springdemo.entities.Unigram;
import com.example.springdemo.repositories.BigramRepository;
import com.example.springdemo.services.MarkovChainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdaptiveMemory {

    private static ArrayList<String> dictionary = readFileInList("src/Files/pass.txt");
    private static ArrayList<String> input = readFileInList("src/Files/pass2.txt");

    @Autowired
    private MarkovChainService markovChainService;



    public static ArrayList<String> getInput() {
        return input;
    }

    public static void setInput(ArrayList<String> input) {
        AdaptiveMemory.input = input;
    }

    public ArrayList<String> getDictionary() {
        return dictionary;
    }


    public static void setDictionary(ArrayList<String> dictionary) {
        AdaptiveMemory.dictionary = dictionary;
    }

    public static ArrayList<String> readFileInList(String path) {
        Scanner s = null;
        try {
            s = new Scanner(new File(path));//"src/Files/pass.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<String> list = new ArrayList<>();
        while (s.hasNext()) {
            list.add(s.next());
        }
        s.close();
        //DictionaryAttack.dictionary = list;
        return list;
    }

    public static ArrayList<String> populate() {
        ArrayList<String> result = new ArrayList<>();
        for (int i = 33; i <= 126; i++) {
            result.add(Character.toString((char) i));
        }
        return result;
    }

    public static int strstr(String X, String Y) {
        // if X is null or if X's length is less than that of Y's
        if (X == null || Y.length() > X.length()) {
            return -1;
        }

        // if Y is null or is empty
        if (Y == null || Y.length() == 0) {
            return 0;
        }

        for (int i = 0; i <= X.length() - Y.length(); i++) {
            int j;
            for (j = 0; j < Y.length(); j++) {
                if (Y.charAt(j) != X.charAt(i + j)) {
                    break;
                }
            }

            if (j == Y.length()) {
                return i;
            }
        }

        return -1;
    }

    public ArrayList<UnigramDTO> unigram() {


        ArrayList<UnigramDTO> lista = new ArrayList<>();
        ArrayList<String> alphabet = populate();
        int m = 0;

        for (int i = 1; i <= alphabet.size(); i++) {
            UnigramDTO elem = new UnigramDTO();
            elem.setSequence(alphabet.get(m++));
            elem.setFrequency(0);
            lista.add(elem);
        }

        try (BufferedReader br = new BufferedReader(new FileReader("src/Files/pass2.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                for (int j = 0; j <line.length(); j++) {
                    String letter1 = Character.toString(line.charAt(j));
                    for (int z = 0; z < alphabet.size(); z++) {
                        if (lista.get(z).getSequence().equals(letter1)) {
                            int contor = lista.get(z).getFrequency();contor++;
                            lista.get(z).setFrequency(contor);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lista;
    }


    public ArrayList<BigramDTO> bigram() {

        ArrayList<BigramDTO> lista = new ArrayList<>();
        ArrayList<String> alphabet = populate();
        ArrayList<String> alphabet2 = new ArrayList<>();


        for (int i = 0; i < alphabet.size(); i++) {
            for (int j = 0; j < alphabet.size(); j++) {
                String aux = (new StringBuilder()).append(alphabet.get(i)).append(alphabet.get(j)).toString();
                alphabet2.add(aux);
            }
        }

        int m = 0;

        for (int i = 1; i <= alphabet2.size(); i++) {
            BigramDTO elem = new BigramDTO();
            elem.setSequence(alphabet2.get(m++));
            elem.setFrequency(0);
            lista.add(elem);
        }


        try (BufferedReader br = new BufferedReader(new FileReader("src/Files/pass2.txt"))) {
            String line;

            while ((line = br.readLine()) != null) {
                for (int j = 0; j <line.length()-1; j++) {
                    String letter1 = Character.toString(line.charAt(j));
                    String letter2 = Character.toString(line.charAt(j + 1));
                    String sequence = letter1 + letter2;
                    for (int z = 0; z < alphabet2.size(); z++) {
                        if (lista.get(z).getSequence().equals(sequence)) {
                            int contor = lista.get(z).getFrequency();contor++;
                            lista.get(z).setFrequency(contor);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("NU");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("NU");
        }
        return lista;
    }




    public ArrayList<TrigramDTO> trigram() {
        ArrayList<TrigramDTO> lista = new ArrayList<>();
        ArrayList<String> alphabet = populate();
        ArrayList<String> alphabet2 = new ArrayList<>();
        ArrayList<String> alphabet3 = new ArrayList<>();

        for (int i = 0; i < alphabet.size(); i++) {
            for (int j = 0; j < alphabet.size(); j++) {
                String aux = (new StringBuilder()).append(alphabet.get(i)).append(alphabet.get(j)).toString();
                alphabet2.add(aux);
            }
        }

        for (int i = 0; i < alphabet.size(); i++) {
            for (int j = 0; j < alphabet.size(); j++) {
                String aux = (new StringBuilder()).append(alphabet.get(i)).append(alphabet.get(j)).toString();
                for (int z = 0; z < alphabet.size(); z++) {
                    String aux2 = aux + alphabet.get(z);
                    alphabet3.add(aux2);
                }

            }
        }


        int m = 0;

        for (int i = 1; i <= alphabet3.size(); i++) {
            TrigramDTO elem = new TrigramDTO();
            elem.setSequence(alphabet3.get(m++));
            elem.setFrequency(0);
            lista.add(elem);
        }


        /*int c = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("src/Files/pass2.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(c++);
                for (int j = 0; j <line.length()-2; j++) {
                    String letter1 = Character.toString(line.charAt(j));
                    String letter2 = Character.toString(line.charAt(j+1));
                    String letter3 = Character.toString(line.charAt(j+2));
                    String sequence = letter1 + letter2 + letter3;
                    for (int z = 0; z < alphabet3.size(); z++) {
                        if (lista.get(z).getSequence().equals(letter1)) {
                            int contor = lista.get(z).getFrequency();contor++;
                            lista.get(z).setFrequency(contor);
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        return lista;
    }


    public Map<String,Double> test()
    {
        Double aux = markovChainService.findFreqSumUnigram();
        System.out.println(aux);
        Map<String,Double> result = new HashMap();

        ArrayList<String> alphabet = populate();
        for(int i=0;i<alphabet.size();i++) {
            Integer aux2 = markovChainService.findFreqBySequenceUnigram(alphabet.get(i));
            Double prob = Double.valueOf(aux2/ aux);
            result.put(alphabet.get(i),++prob);
        }

        return result;
    }

    public Map<String,Double> test2()
    {
        Double aux = markovChainService.findFreqSumBigram();
        System.out.println(aux);
        Map<String,Double> result = new HashMap();

        ArrayList<String> alphabet = populate();

        ArrayList<String> alphabet2 = new ArrayList<>();

        for (int i = 0; i < alphabet.size(); i++) {
            for (int j = 0; j < alphabet.size(); j++) {
                String aux2 = (new StringBuilder()).append(alphabet.get(i)).append(alphabet.get(j)).toString();
                alphabet2.add(aux2);
            }
        }

        for(int i=0;i<alphabet2.size();i++) {
            Integer aux2 = markovChainService.findFreqBySequenceBigram(alphabet2.get(i));
            Double prob = Double.valueOf(aux2/ aux);
            result.put(alphabet2.get(i),++prob);
        }

        return result;
    }


    public static void main(String[] args) {



    }
}

