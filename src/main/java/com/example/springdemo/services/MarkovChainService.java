package com.example.springdemo.services;
import com.example.springdemo.dto.BigramDTO;
import com.example.springdemo.dto.TrigramDTO;
import com.example.springdemo.dto.UnigramDTO;
import com.example.springdemo.dto.builders.BigramBuilder;
import com.example.springdemo.dto.builders.TrigramBuilder;
import com.example.springdemo.dto.builders.UnigramBuilder;
import com.example.springdemo.entities.Bigram;
import com.example.springdemo.entities.Trigram;
import com.example.springdemo.entities.Unigram;
import com.example.springdemo.repositories.BigramRepository;
import com.example.springdemo.repositories.TrigramRepository;
import com.example.springdemo.repositories.UnigramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MarkovChainService {

    @Autowired
    private UnigramRepository unigramRepository;

    @Autowired
    private BigramRepository bigramRepository;

    @Autowired
    private TrigramRepository tigramRepository;

    public Integer findFreqBySequenceUnigram(String sequence) {
        List<Unigram> objects = unigramRepository.findAll();

        for (int i=0;i<objects.size();i++) {
            if(objects.get(i).getSequence().equals(sequence))
                return objects.get(i).getFrequency();
        }
        return 0;
    }

    public Double findFreqSumUnigram(){
        List<Unigram> bruteForce = unigramRepository.findAll();
        Double sum = 0.0;
        for (int i=0;i<bruteForce.size();i++) {
            sum += bruteForce.get(i).getFrequency();
        }
        return sum;
    }

    public Double findFreqSumBigram(){
        List<Bigram> bruteForce = bigramRepository.findAll();
        Double sum = 0.0;
        System.out.println(bruteForce.size());
        for (int i=0;i<bruteForce.size();i++) {
            sum += bruteForce.get(i).getBigramFrequency();
        }
        return sum;
    }

    public Integer findFreqBySequenceBigram(String sequence) {
        List<Bigram> objects = bigramRepository.findAll();
        for (int i=0;i<objects.size();i++) {
            if(objects.get(i).getSequence().equals(sequence))
                return objects.get(i).getBigramFrequency();
        }
        return 0;
    }

    public Double findFreqSumTrigram(){
        List<Trigram> bruteForce = tigramRepository.findAll();
        Double sum = 0.0;
        for (int i=0;i<bruteForce.size();i++) {
            sum += bruteForce.get(i).getFrequency();
        }
        return sum;
    }




    public String insertUnigram (UnigramDTO bruteForceDTO) {

        Unigram brute = new Unigram();
        brute.setSequence(bruteForceDTO.getSequence());
        brute.setFrequency(bruteForceDTO.getFrequency());
        return unigramRepository
                .save(UnigramBuilder.generateEntityFromDTO(bruteForceDTO))
                .getSequence();
    }

    public String insertBigram (BigramDTO bruteForceDTO) {

        Bigram brute = new Bigram();
        brute.setSequence(bruteForceDTO.getSequence());
        brute.setBigramFrequency(bruteForceDTO.getFrequency());
        return bigramRepository
                .save(BigramBuilder.generateEntityFromDTO(bruteForceDTO))
                .getSequence();
    }

    public String insertTrigram(TrigramDTO trigramDTO) {

        Trigram brute = new Trigram();
        brute.setSequence(trigramDTO.getSequence());
        brute.setBigramFrequency(trigramDTO.getFrequency());
        return tigramRepository
                .save(TrigramBuilder.generateEntityFromDTO(trigramDTO))
                .getSequence();
    }





    /*public Integer update(PersonDTO personDTO) {

        Optional<Person> person = personRepository.findById(personDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Person", "user id", personDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return personRepository.save(PersonBuilder.generateEntityFromDTO(personDTO)).getId();
    }

    public void delete(PersonViewDTO personViewDTO){
        this.personRepository.deleteById(personViewDTO.getId());
    }
*/



}

