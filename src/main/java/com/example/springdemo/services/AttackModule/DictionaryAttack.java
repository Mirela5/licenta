package com.example.springdemo.services.AttackModule;

import com.example.springdemo.dto.BruteForceDTO;
import com.example.springdemo.dto.builders.BruteForceBuilder;
import com.example.springdemo.entities.BruteForce;
import com.example.springdemo.repositories.BruteForceRepository;
import com.example.springdemo.services.BruteForceService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Service
public class DictionaryAttack {

    private static ArrayList<String> dictionary = readFileInList("src/Files/pass.txt");
    private static ArrayList<String> input =  readFileInList("src/Files/pass2.txt");;
    private String password;


    public static ArrayList<String> getInput() {
        return input;
    }

    public static void setInput(ArrayList<String> input) {
        DictionaryAttack.input = input;
    }

    public ArrayList<String> getDictionary() {
        return dictionary;
    }



    public static void setDictionary(ArrayList<String> dictionary) {
        DictionaryAttack.dictionary = dictionary;
    }

    public static ArrayList<String> readFileInList(String path) {
        Scanner s = null;
        try {
            s = new Scanner(new File(path));//"src/Files/pass.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<String> list = new ArrayList<>();
        while (s.hasNext()) {
            list.add(s.next());
        }
        s.close();
        //DictionaryAttack.dictionary = list;
        return list;
    }

    public static int LevensteinDistance(String str1, String str2, int m, int n) {
        // Create a table to store results of subproblems
        int dp[][] = new int[m + 1][n + 1];

        // Fill d[][] in bottom up manner
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                // If first string is empty, only option is to
                // insert all characters of second string
                if (i == 0)
                    dp[i][j] = j; // Min. operations = j

                    // If second string is empty, only option is to
                    // remove all characters of second string
                else if (j == 0)
                    dp[i][j] = i; // Min. operations = i

                    // If last characters are same, ignore last char
                    // and recur for remaining string
                else if (str1.charAt(i - 1) == str2.charAt(j - 1))
                    dp[i][j] = dp[i - 1][j - 1];

                    // If the last character is different, consider all
                    // possibilities and find the minimum
                else
                    dp[i][j] = 1 + min(dp[i][j - 1], // Insert
                            dp[i - 1][j], // Remove
                            dp[i - 1][j - 1]); // Replace
            }
        }

        return dp[m][n];
    }

    public static int min(int x, int y, int z) {
        if (x <= y && x <= z)
            return x;
        if (y <= x && y <= z)
            return y;
        else
            return z;
    }

    public BruteForceDTO getStrenghtPassword(String password)
    {
        BruteForceDTO elem = new BruteForceDTO();

        Integer found = 0;


            Instant start = java.time.Instant.now();
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int z = 0; z < dictionary.size(); z++)
            {
                found = 0;


                int j = LevensteinDistance(password, dictionary.get(z), password.length(), dictionary.get(z).length());
                elem.setLevensteinDistance(j);
                if (j == 0) {
                    elem.setHit(1);
                    Instant end = java.time.Instant.now();
                    Duration between = java.time.Duration.between(start, end);
                    elem.setDictionaryScore((between.toMillis()));
                    found = 1;
                    break;
                }
            }
            if (found == 0) {
                elem.setHit(0);
                Instant end = java.time.Instant.now();
                Duration between = java.time.Duration.between(start, end);
                elem.setDictionaryScore((between.toMillis()));
            }

        return elem;
    }

    public static ArrayList<BruteForceDTO> getStrenght(List<String> input, ArrayList<String> dictionary) {

        ArrayList<BruteForceDTO> lista = new ArrayList<>();


        Integer found = 0;
        BruteForceDTO brute;

        for (int i = 0; i < 10; i++)
        {
            System.out.println(input.get(i));
            brute = new BruteForceDTO();
            Instant start = java.time.Instant.now();
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int z = 0; z < dictionary.size(); z++)
            {
                found = 0;


                int j = LevensteinDistance(input.get(i), dictionary.get(z), input.get(i).length(), dictionary.get(z).length());
                brute.setLevensteinDistance(j);
                if (j == 0) {
                    brute.setHit(1);
                    Instant end = java.time.Instant.now();
                    Duration between = java.time.Duration.between(start, end);
                    brute.setDictionaryScore((between.toMillis()));
                    lista.add(brute);
                    found = 1;
                    break;
                }
            }
            if (found == 0) {
                brute.setHit(0);
                Instant end = java.time.Instant.now();
                Duration between = java.time.Duration.between(start, end);
                brute.setDictionaryScore((between.toMillis()));
                lista.add(brute);
            }
        }
        return lista;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static void main(String[] args){

        //dictionary = readFileInList("src/Files/pass.txt");
       // input = readFileInList("src/Files/cain-and-abel.txt");

        //setDictionary(DictionaryAttack.dictionary);
         /*ArrayList<BruteForceDTO> output = getStrenght(input, dictionary);
        for (int i = 0; i < output.size(); i++) {
            System.out.println(output.get(i));
        }*/
    }
}
