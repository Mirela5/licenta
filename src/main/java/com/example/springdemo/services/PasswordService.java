package com.example.springdemo.services;

import com.example.springdemo.dto.PasswordDTO;
import com.example.springdemo.dto.builders.PasswordBuilder;
import com.example.springdemo.entities.BruteForce;
import com.example.springdemo.entities.Password;
import com.example.springdemo.repositories.PasswordRepository;
import com.example.springdemo.services.AttackModule.DictionaryAttack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PasswordService {

    @Autowired
    private PasswordRepository passwordRepository;
    @Autowired
    private DictionaryAttack dictionaryAttack;
    @Autowired
    private BruteForceService bruteForceService;


    public Integer insert(PasswordDTO bruteForceDTO) {

        Password brute = new Password();
        brute.setId(bruteForceDTO.getId());
        brute.setPassword(bruteForceDTO.getPassword());
        brute.setUtilDate(bruteForceDTO.getUtilDate());

        bruteForceService.insert(dictionaryAttack.getStrenghtPassword(brute.getPassword()));

        return passwordRepository
                .save(PasswordBuilder.generateEntityFromDTO(bruteForceDTO))
                .getId();
    }

}
