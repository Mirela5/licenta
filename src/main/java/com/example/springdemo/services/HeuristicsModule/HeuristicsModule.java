package com.example.springdemo.services.HeuristicsModule;

import com.example.springdemo.dto.HeuristicsDTO;
import com.example.springdemo.entities.Heuristics;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class HeuristicsModule {

    private static ArrayList<String> input = readFileInList("src/Files/pass2.txt");
    private static Integer Tmax = 3;
    private static Integer N;
    /**– lowercase letters a-z (size=26)
     – uppercase letters A-Z (size=26)
     – digits 0-9 (size=10)
     – and special characters/symbols (size=32)**/
    private static long NoGmax = (long) Math.pow(10,13);



    public static ArrayList<HeuristicsDTO> getStrength(){

        ArrayList<HeuristicsDTO> result = new ArrayList<>();
        HeuristicsDTO heuristics;
        for (int i = 0; i < input.size(); i++) {

            heuristics = new HeuristicsDTO();
            N = RegexNoSymbols(input.get(i));
            heuristics.setN(N);
            double Lmin = formula(Tmax, N, NoGmax);
            if (input.get(i).length() > Lmin)
                heuristics.setHeuristicsScore(10);
            else
               heuristics.setHeuristicsScore(0);

            heuristics.setTmax(3);
            heuristics.setNoGmax(heuristics.getNoGmax());
            result.add(heuristics);
        }
        return result;
    }

    public static double formula(Integer Tmax, Integer N, long NoGmax)
    {
        double Lmin;
        Lmin = Math.log(NoGmax * Tmax) / Math.log(N);
        return Lmin;
    }

    private static Integer RegexNoSymbols (String input)
    {
        Integer N = 0;
        String patternUppercase = "[A-Z\\s]+";
        String patternLowercase = "[a-z\\s]+";
        String patternDigits = "[0-9\\s]+";
        String patternSymbols ="[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]";

        Pattern Uppercase = Pattern.compile(patternUppercase);
        Pattern Lowercase = Pattern.compile(patternLowercase);
        Pattern Digits = Pattern.compile(patternDigits);
        Pattern Symbols = Pattern.compile(patternSymbols);

        Matcher m1 = Uppercase.matcher(input);
        Matcher m2 = Lowercase.matcher(input);
        Matcher m3 = Digits.matcher(input);
        Matcher m4 = Symbols.matcher(input);

        if (m1.find( )) { N += 26;}
        if (m2.find( )) { N += 26;}
        if (m3.find( )) { N += 10;}
        if (m4.find( )) { N += 32;}

        return N;
    }

    public static ArrayList<String> readFileInList(String path) {
        Scanner s = null;
        try {
            s = new Scanner(new File(path));//"src/Files/pass2.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<String> list = new ArrayList<>();
        while (s.hasNext()) {
            list.add(s.next());
        }
        s.close();
        //DictionaryAttack.dictionary = list;
        return list;
    }

    public static ArrayList<String> getInput() {
        return input;
    }

    public static void setInput(ArrayList<String> input) {
        HeuristicsModule.input = input;
    }

    public Integer getTmax() {
        return Tmax;
    }

    public void setTmax(Integer tmax) {
        Tmax = tmax;
    }

    public Integer getN() {
        return N;
    }

    public static void setN(Integer n) {
       N = n;
    }

    public long getNoGmax() {
        return NoGmax;
    }

    public void setNoGmax(long noGmax) {
        NoGmax = noGmax;
    }

    public static void main(String[] args) {
        //Integer aux = RegexNoSymbols("Pass.word923n,;sd");
        //long i = (long) Math.pow(10,13);
        //System.out.println(Math.log(10));

        /*Integer aux = getStrength("Pas.12",Tmax,N, NoGmax);
        System.out.println(aux);*/
    }
}
