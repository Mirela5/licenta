package com.example.springdemo.services;


import com.example.springdemo.dto.BruteForceDTO;
import com.example.springdemo.dto.HeuristicsDTO;
import com.example.springdemo.dto.builders.BruteForceBuilder;
import com.example.springdemo.dto.builders.HeuristicsBuilder;
import com.example.springdemo.entities.Heuristics;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.HeuristicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HeuristicsService {

    @Autowired
    private HeuristicsRepository heuristicsRepository;

    public HeuristicsDTO findPersonById(Integer id){
        Optional<Heuristics> bruteForce  = heuristicsRepository.findById(id);

        if (!bruteForce.isPresent()) {
            throw new ResourceNotFoundException("Heuristics", "id", id);
        }
        return HeuristicsBuilder.generateDTOFromEntity(bruteForce.get());
    }

    /*public List<HeuristicsDTO> findAll(){
        List<Heuristics> bruteForce = heuristicsRepository.getAllOrdered();

        return bruteForce.stream()
                .map(HeuristicsBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }*/



    public Integer insert(HeuristicsDTO bruteForceDTO) {

        Heuristics brute = new Heuristics();
        brute.setId(bruteForceDTO.getId());
        brute.setN(bruteForceDTO.getN());
        brute.setNoGmax(bruteForceDTO.getNoGmax());
        brute.setTmax(bruteForceDTO.getTmax());
        return heuristicsRepository
                .save(HeuristicsBuilder.generateEntityFromDTO(bruteForceDTO))
                .getId();
    }

}
