package com.example.springdemo.entities;

import lombok.Data;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "trigram")
public class Trigram {

    @Id
    @Column(name = "sequence", unique = true, nullable = false)
    private String sequence;

    @Column(name = "frequency", nullable = true)
    private Integer trigramFrequency;



    public Trigram(String sequence, Integer trigramFrequency) {

        this.sequence = sequence;
        this.trigramFrequency = trigramFrequency;
    }

    public Trigram() { }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        sequence = sequence;
    }

    public void setBigramFrequency(Integer trigramFrequency) {
        this.trigramFrequency = trigramFrequency;
    }

    public Integer getFrequency() {
        return trigramFrequency;
    }
}
