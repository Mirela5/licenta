package com.example.springdemo.entities;

import lombok.Data;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "password")
public class Password {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "password", nullable = true)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    private Date utilDate;


    public Password(Integer id, String password,Date utilDate) {
        this.id = id;
        this.password = password;
        this.utilDate = utilDate;
    }

    public Password(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUtilDate() {
        return utilDate;
    }

    public void setUtilDate(Date utilDate) {
        this.utilDate = utilDate;
    }
}
