package com.example.springdemo.entities;

import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "brute_force")
public class BruteForce {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "dictionary_column", nullable = true)
    private double dictionaryScore;

    @Column(name = "hit_miss", nullable = true)
    private Integer hitMiss;

    @Column(name = "levenstein_distance", nullable = true)
    private Integer levensteinDistance;

    public BruteForce(Integer id, double dictionaryScore, Integer hitMiss, Integer levensteinDistance) {
        this.id = id;
        this.dictionaryScore = dictionaryScore;
        this.hitMiss = hitMiss;
        this.levensteinDistance = levensteinDistance;
    }

    public BruteForce() { }

    public Integer getLevensteinDistance() {
        return levensteinDistance;
    }

    public void setLevensteinDistance(Integer levensteinDistance) {
        this.levensteinDistance = levensteinDistance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public  double getDictionaryScore() {
        return dictionaryScore;
    }

    public void setDictionaryScore(double dictionaryScore) {
        this.dictionaryScore = dictionaryScore;
    }

    public Integer getHit() {
        return hitMiss;
    }

    public void setHit(Integer hit) {
        this.hitMiss = hit;
    }
}
