package com.example.springdemo.entities;

import lombok.Data;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "unigram")
public class Unigram {


    @Id
    @Column(name = "sequence", unique = true, nullable = false)
    private String sequence;

    @Column(name = "frequency", nullable = true)
    private Integer frequency;



    public Unigram(String sequence, Integer frequency) {

        this.sequence = sequence;
        this.frequency = frequency;
    }

    public Unigram() { }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        sequence = sequence;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getFrequency() {
        return frequency;
    }
}
