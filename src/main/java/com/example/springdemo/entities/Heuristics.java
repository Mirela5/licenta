package com.example.springdemo.entities;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "heuristics")

public class Heuristics {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "N", nullable = true)
    private Integer N;

    @Column(name = "NoGmax", nullable = true)
    private long NoGmax;

    @Column(name = "Tmax", nullable = true)
    private Integer Tmax;

    @Column(name = "heuristicsScore", nullable = true)
    private Integer heuristicsScore;

    public Heuristics(Integer id, Integer n, long noGmax, Integer tmax, Integer heuristicsScore) {
        id = id;
        N = n;
        NoGmax = noGmax;
        Tmax = tmax;
        this.heuristicsScore = heuristicsScore;
    }

    public Heuristics(Integer n, long noGmax, Integer tmax, Integer heuristicsScore) {
        N = n;
        NoGmax = noGmax;
        Tmax = tmax;
        this.heuristicsScore = heuristicsScore;
    }

    public Heuristics() {

    }

    public Integer getHeuristicsScore() {
        return heuristicsScore;
    }

    public void setHeuristicsScore(Integer heuristicsScore) {
        this.heuristicsScore = heuristicsScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getN() {
        return N;
    }

    public void setN(Integer n) {
        N = n;
    }

    public long getNoGmax() {
        return NoGmax;
    }

    public void setNoGmax(long noGmax) {
        NoGmax = noGmax;
    }

    public Integer getTmax() {
        return Tmax;
    }

    public void setTmax(Integer tmax) {
        Tmax = tmax;
    }
}
