package com.example.springdemo.entities;

import lombok.Data;
import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "bigram")
public class Bigram {

    @Id
    @Column(name = "sequence", unique = true, nullable = false)
    private String sequence;

    @Column(name = "frequency", nullable = true)
    private Integer bigramFrequency;



    public Bigram(String sequence, Integer bigramFrequency) {

        this.sequence = sequence;
        this.bigramFrequency = bigramFrequency;
    }

    public Bigram() { }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        sequence = sequence;
    }

    public void setBigramFrequency(Integer bigramFrequency) {
        this.bigramFrequency = bigramFrequency;
    }

    public Integer getBigramFrequency() {
        return bigramFrequency;
    }
}
