package com.example.springdemo.seed;

import com.example.springdemo.dto.*;
import com.example.springdemo.entities.Unigram;
import com.example.springdemo.services.AttackModule.DictionaryAttack;
import com.example.springdemo.services.BruteForceService;
import com.example.springdemo.services.HeuristicsModule.HeuristicsModule;
import com.example.springdemo.services.HeuristicsService;
import com.example.springdemo.services.MarkovChain.AdaptiveMemory;
import com.example.springdemo.services.MarkovChainService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;

@Component
//@RequiredArgsConstructor
// The Order ensures that this command line runner is ran first (before the ConsoleController)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class attackSeed {//implements CommandLineRunner {

    private BruteForceService bruteForceService;
    private DictionaryAttack dictionaryAttack;

   private HeuristicsService heuristicsService;
   private HeuristicsModule heuristicsModule;

   private MarkovChainService markovChainService;
   private AdaptiveMemory adaptiveMemory;

    public attackSeed(BruteForceService bruteForceService, DictionaryAttack dictionaryAttack, HeuristicsService heuristicsService, HeuristicsModule heuristicsModule, MarkovChainService markovChainService, AdaptiveMemory adaptiveMemory) {
        this.bruteForceService = bruteForceService;
        this.dictionaryAttack = dictionaryAttack;
        this.heuristicsService = heuristicsService;
        this.heuristicsModule = heuristicsModule;
        this.markovChainService = markovChainService;
        this.adaptiveMemory = adaptiveMemory;
    }

    //@Override
    @Transactional
    public void run(String... args) throws Exception {


        /*ArrayList<UnigramDTO> unigramList = adaptiveMemory.unigram();
        for (int i=0;i<unigramList.size();i++)
               markovChainService.insertUnigram(unigramList.get(i));
*/
        /*System.out.println("Aici");
        ArrayList<BigramDTO> bigramList = adaptiveMemory.bigram();
        System.out.println(bigramList.size());
        for (int i=0;i<bigramList.size();i++)
             markovChainService.insertBigram(bigramList.get(i));*/

         //Double d = markovChainService.findFreqSumBigram();
         //System.out.println(d);
        //Integer aux = markovChainService.findFreqSum();
        //System.out.println(aux);
        //ArrayList<String> input1 = adaptiveMemory.getInput();
      /*  ArrayList<TrigramDTO> brute1 = adaptiveMemory.trigram();
        for (int i = 0; i < brute1.size(); i++) {
           markovChainService.insertTrigram(brute1.get(i));
        }*/
       /*Long aux = markovChainService.findFreqSumUnigram();
       System.out.println(aux);
       Integer aux2 = markovChainService.finfFreqBySequence("!");
       System.out.println(aux2);*/

       /*Map<String,Double> map = adaptiveMemory.test();
       Set set= map.entrySet();
        Iterator itr=set.iterator();
      while(itr.hasNext()){
               Map.Entry entry=(Map.Entry)itr.next();
               System.out.println(entry.getKey()+" "+entry.getValue());
       }*/

        /*ArrayList<BigramDTO> brute2 = adaptiveMemory.bigram(input1);
        for (int i = 0; i < brute2.size(); i++) {
            markovChainService.insertBigram(brute2.get(i));
        }*/

       /* ArrayList<TrigramDTO> brute3 = adaptiveMemory.trigram();
        for (int i = 0; i < brute3.size(); i++) {
            markovChainService.insertTrigram(brute3.get(i));
        }*/

      /*  List<String> aux = markovChainService.findAllFrequencies();
        for (int i = 0; i < aux.size(); i++) {
            System.out.println(aux.get(i));
        }*/

        /*for (int i = 0; i < brute2.size(); i++) {
            markovChainService.insertBigram(brute2.get(i));
        }*/

        ArrayList<String> input = dictionaryAttack.getInput();
        ArrayList<String> dictionary = dictionaryAttack.getDictionary();
        ArrayList<BruteForceDTO> brute = DictionaryAttack.getStrenght(input, dictionary);
        for (int i = 0; i < 10; i++) {
            bruteForceService.insert(brute.get(i));
        }
/*
        ArrayList<HeuristicsDTO> heuristics = heuristicsModule.getStrength();
        for (int i = 0; i < heuristics.size(); i++) {
            heuristicsService.insert(heuristics.get(i));
        }*/

        }
    }
