package com.example.springdemo.repositories;

import com.example.springdemo.entities.BruteForce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BruteForceRepository extends JpaRepository<BruteForce, Integer> {

    Optional<BruteForce> findById(Integer id);

    @Query(value = "SELECT u " +
            "FROM BruteForce u " +
            "ORDER BY u.id")
    List<BruteForce> getAllOrdered();
}

/*
*    Person findByEmail(String email);

    @Query(value = "SELECT u " +
            "FROM Person u " +
            "ORDER BY u.name")
    List<Person> getAllOrdered();


    @Query(value = "SELECT p " +
            "FROM Person p " +
            "INNER JOIN FETCH p.items i"
    )
    List<Person> getAllFetch();
* */
  
