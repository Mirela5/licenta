package com.example.springdemo.repositories;

import com.example.springdemo.entities.Unigram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnigramRepository extends JpaRepository<Unigram, String>{

    //Optional<Unigram> findById(Integer id);

    /*@Query(value = "SELECT u " +
            "FROM am1 u " +
            "ORDER BY u.id")
    List<Unigram> getAllOrdered2();*/
}

