package com.example.springdemo.repositories;

import com.example.springdemo.entities.Bigram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BigramRepository extends JpaRepository<Bigram, String> {


}
