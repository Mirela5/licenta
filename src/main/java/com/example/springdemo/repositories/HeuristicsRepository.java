package com.example.springdemo.repositories;

import com.example.springdemo.entities.Heuristics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HeuristicsRepository extends JpaRepository<Heuristics, Integer> {

    Optional<Heuristics> findById(Integer id);


    /*@Query(value = "SELECT u " +
            "FROM heuristics u " +
            "ORDER BY u.id")
    List<Heuristics> getAllOrdered();*/
}
