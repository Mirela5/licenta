package com.example.springdemo.repositories;

import com.example.springdemo.entities.Trigram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrigramRepository extends JpaRepository<Trigram, String>{
}
